# Story
Our company is maintaining some legacy software for an important customer.
Customer is reporting a bug in calculations it produces.
"Sometimes those darn numbers are just wrong" is the only description we received.

We asked our best developers to have a look and suggest what might be wrong.
They all said that it is a mess and needs to be rewritten from scratch.
Customer denied such solution.

We have hired an external consultant to tell us how to proceed.
After he reviewed our codebase he said that it is strange that system with 98% test coverage
manifests such unpredictable behavior.

He also suggested that there might have been some bad architectural decisions in the past and
that the system should be refactored to be more SOLID.
He also explained the situation to our customer and they are now willing to pay for refactoring time.

Lastly he suggested that he knows the right person for the job and gave us your contact.
Will you please help us out of this pickle?

