package com.company.xxx;

public class Main {

    public static void main(String[] args) {
	    Application app = new Application(new ProductionDatabase());
	    app.run();

        reportChanges(app);
        System.out.println("Sum of areas: " + app.getSumOfAreas());
        System.out.println("Sum of circumferences " + app.getSumOfCircumferences());
    }

    private static void reportChanges (Application app) {
        System.out.println(app.getSumOfCounters() + " changes in width occurred");
    }
}
