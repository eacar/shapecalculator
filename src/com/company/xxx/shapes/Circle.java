package com.company.xxx.shapes;

public class Circle implements Shape{

    private Double r;

    public Circle(Double r) {
        this.r = r;
    }

    @Override
    public Double calculateCircumference() {
        return 2 * Math.PI * r;
    }

    @Override
    public Double calculateArea() {
        return Math.PI * r * r;
    }
}
