package com.company.xxx.shapes;

public interface Shape {

    Double calculateCircumference();

    Double calculateArea();
}
