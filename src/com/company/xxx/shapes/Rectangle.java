package com.company.xxx.shapes;

public class Rectangle implements Shape {

    private Double width;
    private Double height;
    private int counter;

    public Rectangle(Double width, Double height) {
        this.width = width;
        this.height = height;
        counter = 0;
    }

    @Override
    public Double calculateCircumference() {
        return 2 * width + 2 * height;
    }

    @Override
    public Double calculateArea() {
        return width * height;
    }

    public Double getWidth() {
        return width;
    }

    public Double getHeight() {
        return height;
    }

    public void setWidth(Double width) {
        counter++;
        this.width = width;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public int getCounter() {
        return counter;
    }

    public void swapDimensions() {
        Double temp = width;

        //If the dimension swaps should also be counted as width changes in business logic, these statements should call
        //the setter methods as shown below, instead of changing the values directly.
        //
        // setWidth(height);
        // setHeight(temp);

        width = height;
        height = temp;
    }
}
