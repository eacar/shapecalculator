package com.company.xxx.shapes;

public class Square extends Rectangle {

    public Square(Double a) {
        super(a, a);
    }

    /**
     * Ensures the square stays as a square even after someone tries to alter its dimensions.
     */

    @Override
    public void setWidth(Double width) {
        super.setWidth(width);
        super.setHeight(width);
    }

    @Override
    public void setHeight(Double height) {
        super.setWidth(height);
        super.setHeight(height);
    }
}
