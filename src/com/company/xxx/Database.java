package com.company.xxx;

import com.company.xxx.shapes.Shape;

import java.util.List;

public interface Database {

    List<Shape> findShapes();
}
