package com.company.xxx.test;

import com.company.xxx.shapes.Circle;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleTest {

    @Test
    public void calculateCircumference() {
        Circle c = new Circle(10d);
        assertEquals(Double.valueOf(62.83185307179586), c.calculateCircumference());
    }

    @Test
    public void calculateArea() {
        Circle c = new Circle(10d);
        assertEquals(Double.valueOf(314.1592653589793), c.calculateArea());
    }
}