package com.company.xxx.test;

import com.company.xxx.shapes.Rectangle;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RectangleTest {

    @Test
    public void calculateCircumference() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        assertEquals(Double.valueOf(12), rectangle.calculateCircumference());
        rectangle.setWidth(2d);
        assertEquals(Double.valueOf(14), rectangle.calculateCircumference());
    }

    @Test
    public void calculateArea() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        assertEquals(Double.valueOf(5.0), rectangle.calculateArea());
        rectangle.setWidth(2d);
        assertEquals(Double.valueOf(10), rectangle.calculateArea());
    }

    @Test
    public void getWidth() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        assertEquals(Double.valueOf(1), rectangle.getWidth());
    }

    @Test
    public void getHeight() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        assertEquals(Double.valueOf(5), rectangle.getHeight());
    }

    @Test
    public void setWidth() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        rectangle.setWidth(2d);
        assertEquals(Double.valueOf(2), rectangle.getWidth());
        assertEquals(1, rectangle.getCounter());
    }

    @Test
    public void setHeight() {
        Rectangle rectangle = new Rectangle(1d, 5d);
        rectangle.setHeight(2d);
        assertEquals(Double.valueOf(2), rectangle.getHeight());
    }

    @Test
    public void getCounter(){
        Rectangle rectangle = new Rectangle(1d,2d);
        rectangle.setWidth(4d);
        rectangle.setWidth(3d);
        rectangle.setWidth(2d);
        assertEquals(3,rectangle.getCounter());
    }
}