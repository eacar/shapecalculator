package com.company.xxx.test;

import com.company.xxx.shapes.Square;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SquareTest {

    @Test
    public void calculateCircumference() {
        Square s = new Square(10d);
        assertEquals(Double.valueOf(40), s.calculateCircumference());
    }

    @Test
    public void calculateArea() {
        Square s = new Square(10d);
        assertEquals(Double.valueOf(100), s.calculateArea());
    }
}