package com.company.xxx.test;

import com.company.xxx.Application;
import com.company.xxx.Database;
import com.company.xxx.shapes.Circle;
import com.company.xxx.shapes.Rectangle;
import com.company.xxx.shapes.Shape;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ApplicationTest {

    @Test
    public void run() {
        Application app = new Application(new Database() {
            @Override
            public List<Shape> findShapes() {
                return Arrays.asList(
                        new Rectangle(1d, 2d),
                        new Circle(1d)
                );
            }
        });

        app.run();

        assertEquals(BigDecimal.valueOf(5.141592653589793), app.getSumOfAreas());
        assertEquals(BigDecimal.valueOf(12.283185307179586), app.getSumOfCircumferences());
    }

    @Test
    public void reportChanges(){

        Rectangle rectangle = new Rectangle(1d, 2d);

        Application app = new Application(new Database() {
            @Override
            public List<Shape> findShapes() {
                return Arrays.asList(
                        rectangle,
                        new Circle(1d)
                );
            }
        });

        rectangle.setWidth(5d);
        rectangle.setWidth(2d);
        rectangle.setWidth(3d);

        app.run();

        assertEquals(3,app.getSumOfCounters());
    }
}