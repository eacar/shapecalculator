package com.company.xxx;

import com.company.xxx.shapes.Circle;
import com.company.xxx.shapes.Rectangle;
import com.company.xxx.shapes.Shape;
import com.company.xxx.shapes.Square;

import java.math.BigDecimal;
import java.util.List;

public class Application {

    private Database database;

    private BigDecimal sumOfAreas = new BigDecimal(0);
    private BigDecimal sumOfCircumferences = new BigDecimal(0);
    private int sumOfCounters = 0;

    public Application(Database database) {
        this.database = database;
    }

    public void run() {
        for (Shape shape : loadShapesFromDatabase()) {

            sumOfCircumferences = sumOfCircumferences.add(BigDecimal.valueOf(shape.calculateCircumference()));

            if (shape instanceof Square) {
                // We have a business rule stating that square areas should count as double the actual area
                sumOfAreas = sumOfAreas.add(BigDecimal.valueOf(shape.calculateArea() * 2));
                sumOfCounters += ((Square) shape).getCounter();
            } else if (shape instanceof Rectangle) {
                // We know that in the database we have some corrupted data
                // so each rectangles width and height must be swapped before calculation
                // just to be sure
                ((Rectangle) shape).swapDimensions();
                sumOfAreas = sumOfAreas.add(BigDecimal.valueOf(shape.calculateArea()));
                sumOfCounters += ((Rectangle) shape).getCounter();
            } else if (shape instanceof Circle) {
                sumOfAreas = sumOfAreas.add(BigDecimal.valueOf(shape.calculateArea()));
            }
        }
    }

    public BigDecimal getSumOfAreas() {
        return sumOfAreas;
    }

    public BigDecimal getSumOfCircumferences() {
        return sumOfCircumferences;
    }

    public int getSumOfCounters() {
        return sumOfCounters;
    }

    private List<Shape> loadShapesFromDatabase() {
        return database.findShapes();
    }
}
